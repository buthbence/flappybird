package com.buthbence.flappybird;

/**
 * Created by soran on 2018. 04. 15..
 */

public class HighScore {
    private int[] scores;
    private String[] names;

    //TODO load from XML
    public HighScore() {
        this.scores = new int[10];
        this.scores[0] = 12;
        this.scores[1] = 10;
        this.scores[2] = 9;
        this.scores[3] = 8;
        this.scores[4] = 6;
        this.scores[5] = 5;
        this.scores[6] = 4;
        this.scores[7] = 3;
        this.scores[8] = 2;
        this.scores[9] = 0;

        this.names = new String[10];
        names[0] = "ABC";
        names[1] = "DEF";
        names[2] = "GHI";
        names[3] = "JKL";
        names[4] = "MNO";
        names[5] = "PQR";
        names[6] = "STU";
        names[7] = "VWX";
        names[8] = "YZA";
        names[9] = "BCD";

    }

    public int getRank(int score) {
        int i = 0;
        for (i = 0; i < 10 && score <= scores[i]; i++)
            ;
        return i + 1;
    }

    public String getName(int i) {
        return names[i];
    }

    public int getScore(int i){
        return scores[i];
    }

    public void saveScore(int score, String name) {
        //TODO implement xml save
        int rank = getRank(score) - 1;
        for (int i = 9; i > rank; i--) {
            scores[i] = scores[i-1];
            names[i] = names[i-1];
        }
        scores[rank] = score;
        names[rank] = name;
    }
}
