package com.buthbence.flappybird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by soran on 2018. 04. 15..
 */

public class HighScoreScreen implements Screen {

    private FlappyBird flappyBird;
    private BitmapFont font;
    SpriteBatch batch;

    public HighScoreScreen(FlappyBird flappyBird) {
        this.flappyBird = flappyBird;
    }

    @Override
    public void show() {
        batch = new SpriteBatch();
        font = new BitmapFont();
        font.setColor(Color.WHITE); // score color
        font.getData().setScale(4); // score size
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        font.draw(batch, "Rank", 10, Gdx.graphics.getHeight()); // top left
        font.draw(batch, "Name", 210, Gdx.graphics.getHeight());
        font.draw(batch, "Score", 600, Gdx.graphics.getHeight());
        HighScore hs = flappyBird.getHighScore();
        for(int i=0;i<10;i++){
            font.draw(batch,Integer.toString(i+1),10,Gdx.graphics.getHeight()-60-50*i);
            font.draw(batch,hs.getName(i),210,Gdx.graphics.getHeight()-60-50*i);
            font.draw(batch,Integer.toString(hs.getScore(i)),600,Gdx.graphics.getHeight()-60-50*i);
        }
        batch.end();
        if(Gdx.input.justTouched())
            flappyBird.setGameScreen();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
