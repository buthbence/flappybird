package com.buthbence.flappybird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by soran on 2018. 04. 15..
 */

public class MenuScreen implements Screen {
    private FlappyBird flappyBird;
    SpriteBatch batch;
    BitmapFont font;


    public MenuScreen(FlappyBird flappyBird) {
        this.flappyBird = flappyBird;
    }

    @Override
    public void show() {
        batch = new SpriteBatch();
        font = new BitmapFont();
        font.setColor(Color.WHITE); // score color
        font.getData().setScale(8); // score size
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        font.draw(batch,"New Game", 200,Gdx.graphics.getHeight() /2 + 100);
        font.draw(batch,"High Scores", 200,Gdx.graphics.getHeight() /2 - 100);
        batch.end();

        if (Gdx.input.justTouched()) { // restart
            if(Gdx.input.getY() < Gdx.graphics.getHeight() / 2)
                flappyBird.setGameScreen();
            else
                flappyBird.setHighScoreScreen();

        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
