package com.buthbence.flappybird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import java.util.Random;

/**
 * Created by soran on 2018. 04. 15..
 */

public class GameScreen implements Screen {

    public GameScreen(FlappyBird flappyBird) {
        this.flappyBird = flappyBird;
    }

    private FlappyBird flappyBird;

    Input.TextInputListener textInputListener;

    SpriteBatch batch;
    Texture background;
    String playerName = "";
    //ShapeRenderer shapeRenderer; // its like batch (batch drawing textures), while shaperenderer drawing shapes. Need cuz of testing.

    Texture gameover;

    // birds things
    Texture[] birds;
    int flapstate = 0; //
    float birdY = 0; //y pos of the bird
    float velocity = 0; // elmozdulas
    Circle birdCircle;
    int score = 0; // Mennyi csovon jutott at.
    int scoringTube = 0;
    BitmapFont font;
    BitmapFont font2;
    //for the game
    int gameState = 0; // touch or not touch
    float gravity = 2; // ezzel csokkentem az velocityt
    //tube things
    Texture topTube;
    Texture bottomTube;
    float gap = 400; // ket tube kozti res
    float maxTubeOffset;
    Random randomGenerator; // to tubeOffset
    float tubeVelocity = 4; // speed of the tube.
    int numberOfTubes = 4;
    float[] tubeX = new float[numberOfTubes]; // tube x coord
    float[] tubeOffset = new float[numberOfTubes]; // randomize the tube location on y coord
    float distanceBetweenTubes;
    Rectangle[] topTubeRectangles;
    Rectangle[] bottomTubeRectangles;

    @Override
    public void show() {
        //background
        batch = new SpriteBatch();
        //shapeRenderer = new ShapeRenderer();
        background = new Texture("bg.png");
        gameover = new Texture("gameover.png");
        //birds
        birds = new Texture[2];
        birds[0] = new Texture("bird.png");
        birds[1] = new Texture("bird2.png");
        birdCircle = new Circle();
        font = new BitmapFont();
        font.setColor(Color.WHITE); // score color
        font.getData().setScale(10); // score size

        font2 = new BitmapFont();
        font2.setColor(Color.RED);
        font2.getData().setScale(4);

        //tubes
        topTube = new Texture("toptube.png");
        bottomTube = new Texture("bottomtube.png");
        maxTubeOffset = Gdx.graphics.getHeight() / 2 - gap / 2 - 100;
        randomGenerator = new Random();
        distanceBetweenTubes = Gdx.graphics.getWidth() * 3 / 4;
        topTubeRectangles = new Rectangle[numberOfTubes];
        bottomTubeRectangles = new Rectangle[numberOfTubes];

        textInputListener = new Input.TextInputListener() {
            private String text;
            @Override
            public void input(String text) {
                this.text = text;
            }

            @Override
            public void canceled() {

            }

            public String getText(){
                return text;
            }
        };


        startGame();
    }

    public void startGame() {

        birdY = Gdx.graphics.getHeight() / 2 - birds[0].getHeight() / 2;

        for (int i = 0; i < numberOfTubes; i++) {

            tubeOffset[i] = (randomGenerator.nextFloat() - 0.5f) * (Gdx.graphics.getHeight() - gap - 200); // pos of the tubes.

            tubeX[i] = Gdx.graphics.getWidth() / 2 - topTube.getWidth() / 2 + Gdx.graphics.getWidth() + i * distanceBetweenTubes; // first center, second four and a half away

            topTubeRectangles[i] = new Rectangle();
            bottomTubeRectangles[i] = new Rectangle();
        }

    }

    @Override
    public void render(float delta) {

        // drawing
        batch.begin();
        batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight()); // pos of the background

        //Touching examination
        if (gameState == 1) { // game is active


            if (tubeX[scoringTube] < Gdx.graphics.getWidth() / 2) {

                score++;
                if (scoringTube < numberOfTubes - 1) { //0,1,2,3
                    scoringTube++;

                } else {

                    scoringTube = 0;
                }

            }

            if (Gdx.input.justTouched()) { // ide tudod majd irni a szamlalot. Minel kisebb a velocity annal tobbszor kell nyomkodni h fent maradjon a madar.

                velocity = -30;


            }
            for (int i = 0; i < numberOfTubes; i++) {
                //get back the tubes to the beginning
                if (tubeX[i] < -bottomTube.getWidth()) {

                    tubeOffset[i] = (randomGenerator.nextFloat() - 0.5f) * (Gdx.graphics.getHeight() - gap - 200); // pos of the tubes.
                    tubeX[i] += numberOfTubes * distanceBetweenTubes;

                } else {

                    tubeX[i] -= tubeVelocity;

                }

                batch.draw(topTube, tubeX[i], Gdx.graphics.getHeight() / 2 + gap / 2 + tubeOffset[i]);
                batch.draw(bottomTube, tubeX[i], Gdx.graphics.getHeight() / 2 - gap / 2 - bottomTube.getHeight() + tubeOffset[i]);

                topTubeRectangles[i] = new Rectangle(tubeX[i], Gdx.graphics.getHeight() / 2 + gap / 2 + tubeOffset[i], topTube.getWidth(), topTube.getHeight());
                bottomTubeRectangles[i] = new Rectangle(tubeX[i], Gdx.graphics.getHeight() / 2 - gap / 2 - bottomTube.getHeight() + tubeOffset[i], bottomTube.getWidth(), bottomTube.getHeight());

            }

            if (birdY > 0) {
                velocity += gravity; // elmozdulas szamol
                birdY -= velocity; // elmozdulas

            } else {

                gameState = 2;

            }

        } else if (gameState == 0) { // game isnt active (thats the begining state)

            if (Gdx.input.justTouched()) {

                gameState = 1;

            }
        } else if (gameState == 2) { // gameover

            batch.draw(gameover, Gdx.graphics.getWidth() / 2 - gameover.getWidth() / 2, Gdx.graphics.getHeight() / 2 - gameover.getHeight() / 2);

            int rank = flappyBird.getHighScore().getRank(score);
            if (rank < 11) {
                font2.draw(batch, "You have high score! Rank: " + Integer.toString(rank), 100.f, Gdx.graphics.getHeight() / 2 - 40);
            // TODO    Gdx.input.getTextInput(textInputListener, "Enter your name!", "", "");

                if (Gdx.input.justTouched()) { // restart
                    flappyBird.getHighScore().saveScore(score,flappyBird.getPlayerName());
                    flappyBird.setHighScoreScreen();
                }
            }
            else
            if (Gdx.input.justTouched()) { // restart

                //     gameState = 1;
                //     startGame();
                //     score = 0;
                //     scoringTube = 0;
                //     velocity = 0;

                flappyBird.setMenuScreen();

            }

        }

        // change between two png bird1 - bird2
        if (flapstate == 0) {
            flapstate = 1;
        } else {
            flapstate = 0;
        }

        batch.draw(birds[flapstate], Gdx.graphics.getWidth() / 2 - birds[flapstate].getWidth() / 2, birdY); // pos of the bird
        font.draw(batch, String.valueOf(score), 100, 200); // bottom left

        batch.end();

        birdCircle.set(Gdx.graphics.getWidth() / 2, birdY + birds[flapstate].getHeight() / 2, birds[flapstate].getWidth() / 2); //x,y,rad


        //shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        //shapeRenderer.setColor(Color.RED);
        //shapeRenderer.circle(birdCircle.x,birdCircle.y,birdCircle.radius);

        for (int i = 0; i < numberOfTubes; i++) {

            //shapeRenderer.rect(tubeX[i],Gdx.graphics.getHeight() / 2 + gap / 2 + tubeOffset[i], topTube.getWidth(),topTube.getHeight());
            //shapeRenderer.rect(tubeX[i],Gdx.graphics.getHeight() / 2 - gap / 2 - bottomTube.getHeight() + tubeOffset[i], bottomTube.getWidth(),bottomTube.getHeight());

            if (Intersector.overlaps(birdCircle, topTubeRectangles[i]) || Intersector.overlaps(birdCircle, bottomTubeRectangles[i])) {

                gameState = 2;

            }
        }
        //shapeRenderer.end(); // shaperenderer-t ellenorzesre hasznaltam.
    }

    @Override
    public void resize(int width, int height) {
        ;
    }

    @Override
    public void pause() {
        ;
    }

    @Override
    public void resume() {
        ;
    }

    @Override
    public void hide() {
        ;
    }

    @Override
    public void dispose() {
        ;
    }
}
