package com.buthbence.flappybird;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import java.util.Random;

public class FlappyBird extends Game implements ApplicationListener {

    private Screen gameScreen;
    private Screen highScoreScreen;
    private Screen menuScreen;
    private String playerName;
    private HighScore highScore;

    public String getPlayerName() {
        return playerName;
    }

    public HighScore getHighScore() {
        return highScore;
    }

    @Override
    public void create() {
        highScore = new HighScore();
        playerName = "Player";
        setMenuScreen();
    }

    public void setGameScreen() {
        this.gameScreen = new GameScreen(this);
        setScreen(gameScreen);
    }

    public void setMenuScreen() {
        this.menuScreen = new MenuScreen(this);
        setScreen(menuScreen);
    }

    public void setHighScoreScreen() {
        this.highScoreScreen = new HighScoreScreen(this);
        setScreen(highScoreScreen);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        super.render();
    }
}
